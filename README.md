noflo-eda explores the idea of high-level component-based
creation of electronics hardware.

Status
=======
Experiment

Milestones
==========
* 0.0.1: Can sew together a PCB from a couple of components wired together
* 0.1.0: Can be used to create a useful PCB for a real project

License
========
MIT
