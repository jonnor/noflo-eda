
Prototyping ideas
===================
* Create a couple of circuits to be used as "components" on Upverter
* Tag/declare the input and output "ports", either in
* Read in JSON format exports from Upverter components
* Create a new JSON file with all component instances included, and nets between components connected
* Re-upload to Upverter?
* Do auto-layout of components, auto-tracing for component connections
* Get Gerber files out for PCB production


Electronics system architecture
------------------------------
When designing complex electronics systems, one spends a fair bit of time
on the overall architecture. As all decisions flow from this, it is important
to get concensus and nail down this design before continuing with others.

At this level one is mostly interested in the large functional components,
how they are connected (power and control-signals), and to get a feel for
it will work overall.

If one could draw this as a graph of components in Flowhub, it would provide
for a top-level view that one can collaborate on and drill down to details on,
and that one can visualize and play with interactively. It should be possible
to simulate a whole system!

Components should include
* Power supplies / media converters
* Embedded Linux devices (running NoFlo Node.js)
* Desktop/mobile clients (running NoFlo browser)
* Microcontrollers/boards (running MicroFlo)
* Perphiperhals like LEDs
